import sys
from transforms import rotate_right, mirror, blur, greyscale, copy_img
from images import read_img, write_img


def main(img, transform_f):
    image = read_img(img)
    copy = copy_img(image)

    # Aplicamos la funcion de transformación a la imagen
    if transform_f == 'rotate_right':
        transf = rotate_right(copy)
    elif transform_f == 'mirror':
        transf = mirror(copy)
    elif transform_f == 'blur':
        transf = blur(copy)
    elif transform_f == 'greyscale':
        transf = greyscale(copy)

    # Obtenemos el nombre del archivo de salida
    nombre_f, extension = img.rsplit('.', 1)
    nombre_f_trans = f"{nombre_f}_trans.{extension}"

    # Guardamos la imagen transformada en nuevo archivo
    write_img(transf, nombre_f_trans)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Use: python3 transform_simple.py <nombre_archivo> <transformación>')
        sys.exit(1)

    img_name = sys.argv[1]
    transformacion = sys.argv[2]

    trans_validas = ['rotate_right', 'mirror', 'blur', 'greyscale']
    if transformacion not in trans_validas:
        print('Transformación no válida. Intentelo de nuevo.')
        sys.exit(1)

    main(img_name, transformacion)
