import sys
from transforms import (change_colors, rotate_colors, shift, copy_img, filter, rotate_right, mirror, blur,
                        greyscale, crop)
from images import read_img, write_img


def main(image, transformations):
    copy = copy_img(image)  # Copiamos la imagen para no utilizar la original
    i = 0  # Creamos un indice para así recorrer las transformaciones en el bucle

    while i < len(transformations):
        transform_f = transformations[i]
        # Aquí aplicamos primero las transformaciones sin argumentos, aumentamos el indice para
        # pasar de transformacion en transformacion
        if transform_f == 'rotate_right':
            copy = rotate_right(copy)
            i += 1
        elif transform_f == 'mirror':
            copy = mirror(copy)
            i += 1
        elif transform_f == 'blur':
            copy = blur(copy)
            i += 1
        elif transform_f == 'greyscale':
            copy = greyscale(copy)
            i += 1
        # A partir de aqui aplicamos las transformaciones con argumentos
        elif transform_f == 'filter':
            if i + 3 > len(transformations):
                print('Número incorrecto de argumentos para filter.')
                sys.exit(1)
            r, g, b = map(float, transformations[i + 1:i + 4])
            copy = filter(copy, r, g, b)
            i += 4
        elif transform_f == 'crop':
            if i + 4 > len(transformations):
                print('Número incorrecto de argumentos para crop.')
                sys.exit(1)
            x, y, width, height = map(int, transformations[i + 1: i + 5])
            copy = crop(copy, x, y, width, height)
            i += 5
        elif transform_f == 'change_colors':
            # con i + 4 queremos comprobar si hay argumentos suficientes para la transformacion
            if i + 4 > len(transformations):
                print('Número incorrecto de argumentos para change_colors.')
                sys.exit(1)
            to_change = tuple(map(int, transformations[i + 1:i + 4]))
            to_change_to = tuple(map(int, transformations[i + 4:i + 7]))
            copy = change_colors(copy, to_change, to_change_to)
            i += 7
            # Se pone como indice 7 ya que los argumentos totales que utiliza esta transformacion es un total de 7
        elif transform_f == 'rotate_colors':
            # Para este, es lo mismo que change_colors, verificamos que haya los argumentos necesarios
            if i + 2 > len(transformations):
                print('Número incorrecto de argumentos para rotate_colors.')
                sys.exit(1)
            increment = int(transformations[i + 1])
            copy = rotate_colors(copy, increment)
            i += 2
        elif transform_f == 'shift':
            # Comparacion de argumentos
            if i + 3 > len(transformations):
                print('Número incorrecto de argumentos para shift.')
                sys.exit(1)
            horizontal, vertical = map(int, transformations[i + 1:i + 3])
            copy = shift(copy, horizontal, vertical)
            i += 3
        else:
            print(f'Transformación no válida: {transform_f}.')
            sys.exit(1)

    return copy


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Uso: python3 transform_multi.py <nombre_archivo> <transformación1> '
              '[<argumentos1>] <transformación2> ...')
        sys.exit(1)

    img_name = sys.argv[1]
    transformations = sys.argv[2:]

    image = read_img(img_name)
    transformed_image = main(image, transformations)

    # Obtenemos el nombre del archivo de salida
    n_salida, ext = img_name.rsplit('.', 1)
    n_salida_trans = f"{n_salida}_trans.{ext}"

    # Guardamos la imagen transformada en un nuevo archivo
    write_img(transformed_image, n_salida_trans)
