# ENTREGA CONVOCATORIA ENERO
# Zaira Guadalupe Alama Loja, zg.alama.2020@alumnos.urjc.es


*Requisitos mínimos:*
Realización de los tres programas: 'transform_simple', 'transform_args' y 'transform_multi'.
Programa de transformaciones: 'Transforms'
Transformaciones (mñetodos) creados en el anterior programa: change_colors, rotate_right, crop, filter, mirror, shift,
gresycale, blur, copy_img
Enlace de Youtube:
https://youtu.be/2R-AzE2dWNU?si=udDLcEpKg0nX0J8C