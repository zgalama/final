from images import size, create_blank


def change_colors(image: list[list[tuple[int, int, int]]],
                  to_change: tuple[int, int, int],
                  to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    # Produce una nueva imagen con colores cambiados, donde los parametros son: imagen original
    # el color a cambiar y el color que será cambiado, devuelve una imagen.

    # Copiamos la imagen para evitar modificar la original
    new = [list(pix) for pix in image]

    # Iteramos sobre cada pixel de la imagen
    for pix in range(len(new)):
        for col in range(len(new[pix])):
            # Comparamos el color del pixel actual con el color a cambiar
            if new[pix][col] == to_change:
                # Si el color coincide, cambiamos por el nuevo color
                new[pix][col] = to_change_to

    return new


def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    # Nueva imagen girada 90 grados hacia la derecha.
    height, width = size(image)

    # Creamos una imagen con dimensiones intercambiadas para girarla
    rotate_img = create_blank(width, height)

    # Rotamos la imagen
    for pix in range(height):
        for col in range(width):
            rotate_img[col][height - 1 - pix] = image[pix][col]

    return rotate_img


def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    # Producirá una nueva imagen espejo según un eje vertical situado en la mitad de la img.
    height, width = size(image)

    # Creamos una nueva imagen espejo
    mirror_img = [[(0, 0 , 0) for _ in range(width)] for _ in range(height)]

    # Hacemos espejo verticalmente
    for pix in range(height):
        for col in range(width):
            mirror_img[pix][col] = image[height - 1 - pix][col]

    return mirror_img


def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    # Nueva imagen con los colores cambiados, para esto se suma cada componente RGB el num que se le
    # indique, teniendo en cuenta que si el valor llega a 255 tendrá que volver a 0 (0-256).
    # El incremento también puede ser negativo. 2 parametros: img org y incremento, devuelve img.
    rotate_img = [[(0, 0, 0) for _ in range(len(image[pix]))] for pix in range(len(image))]

    # Rotamos los colores
    for pix in range(len(image)):
        for col in range(len(image[pix])):
            r = (image[pix][col][0] + increment) % 256
            g = (image[pix][col][1] + increment) % 256
            b = (image[pix][col][2] + increment) % 256
            rotate_img[pix][col] = (r, g, b)

    return rotate_img


def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    # Nueva img donde el valor RGB de cada pixel será el valor medio de los valores RGB de los
    # pixels que tiene encima, debajo, a la dcha y izqda (en los bordes no se considerá los q sean obvios)
    height, width = size(image)

    blur_img = create_blank(height, width)

    for pix in range(height):
        for col in range(width):
            total_pix = 0
            total_r = 0
            total_g = 0
            total_b = 0

            # Aqui vemos los vecinos
            for p_no, c_no in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                new_pix = pix + p_no
                new_col = col + c_no

                if 0 <= new_pix < height and 0 <= new_col < width:
                    total_pix += 1
                    total_r += image[new_pix][new_col][0]
                    total_g += image[new_pix][new_col][1]
                    total_b += image[new_pix][new_col][2]

            if total_pix > 0:
                avg_r = total_r // total_pix
                avg_g = total_g // total_pix
                avg_b = total_b // total_pix
                blur_img[pix][col] = (avg_r, avg_g, avg_b)

    return blur_img


def shift(image: list[list[tuple[int, int, int]]] , horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    # Desplaza la img el num de pixeles que se indique, en el eje horiz o vertical
    height, width = size(image)

    shift_img = [[(0, 0, 0) for _ in range(width)] for _ in range(height)]

    for pix in range(height):
        for col in range(width):
            new_pix = (pix + vertical) % height
            new_col = (col + horizontal) % width
            shift_img[new_pix][new_col] = image[pix][col]

    return shift_img


def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:
    # Crea una nueva img que contendrá solo los pix que se encuentren dentro de un rectangulo que se especifique.
    # 5 parametros: img a recortar, los otros 4 para especificar el rectangulo a recortar, con coordenadas x, y

    crop_img = []

    for pix in range(y, y + height):
        crop_pix = image[pix][x:x + width]
        crop_img.append(crop_pix)

    return crop_img


def greyscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    # Crea una nueva omg que contendrá la original pero en escala de grises. Para calcular la escala
    # de grises, basta con calcular la media de los tres valores de la tupla RGB y asignar ese valor
    # a los tres valores de la tupla RGB del pixel equivalente en la img resultante.
    greyscale_img = []

    for pix in range(len(image)):
        grayscale_pix = []
        for col in range(len(image[pix])):
            r, g, b = image[pix][col]
            # Calculamos el valor promedio de los componentes RGB para obtener el tono gris
            avg_gray = (r + g + b) // 3
            # Creamos un pixel en escala de grises con el valor promedio
            grayscale_new = (avg_gray, avg_gray, avg_gray)
            grayscale_pix.append(grayscale_new)
        greyscale_img.append(grayscale_pix)

    return greyscale_img


def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:

    # Crea una nueva img que aplica un filtro a todos los pix. Es un multiplicador para cada valor
    # RGB. Cada pix de la img resultante tendrá una tupla RGB igual a la de la img orig.
    # con cada componente RGB multiplicado por su multiplicador corresp., teniendo en cuenta que
    # si el resultado es mayor que el valor max posible para el compom. el valor max posible
    # (255). Devuelve una img.
    filter_img = []

    for pix in range(len(image)):
        filter_pix = []
        for col in range(len(image[pix])):
            # Multiplicamos cada componente RGB por su multiplicador correspondiente
            new_r = min(int(image[pix][col][0] * r), 255)
            new_g = min(int(image[pix][col][1] * g), 255)
            new_b = min(int(image[pix][col][2] * b), 255)
            filter_new = (new_r, new_g, new_b)
            filter_pix.append(filter_new)
        filter_img.append(filter_pix)

    return filter_img


def copy_img(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    width, height = size(image)

    new_img = create_blank(width, height)

    for pix in range(width):
        for col in range(height):
            new_img[pix][col] = image[pix][col]

    return new_img

