import sys
from transforms import change_colors, rotate_colors, shift, copy_img, rotate_right, mirror, blur, greyscale, filter, crop
from images import read_img, write_img


def main(img, transform_f, *args):
    image = read_img(img)
    copy = copy_img(image)

    if transform_f == 'change_colors':
        if len(args) != 6:
            print('Número incorrecto de argumentos para change_colors.')
            sys.exit(1)
        to_change = (args[0], args[1], args[2])
        to_change_to = (args[3], args[4], args[5])
        transform_img = change_colors(copy, to_change, to_change_to)
    elif transform_f == 'rotate_colors':
        if len(args) != 1:
            print('Número incorrecto de argumentos para rotate_colors.')
            sys.exit(1)
        increment = args[0]
        transform_img = rotate_colors(copy, increment)
    elif transform_f == 'shift':
        if len(args) != 2:
            print('Número incorrecto de argumentos para shift.')
            sys.exit(1)
        horizontal, vertical = args
        transform_img = shift(copy, horizontal, vertical)
    elif transform_f == 'filter':
        if len(args) != 3:
            print('Número incorrecto de argumentos para filter.')
            sys.exit(1)
        r, g, b = args
        transform_img = filter(copy, r, g, b)
    elif transform_f == 'crop':
        if len(args) != 4:
            print('Número incorrecto de argumentos para filter.')
            sys.exit(1)
        x, y, width, height = args
        transform_img = crop(copy, x, y, width, height)
    else:
        print('Transformación no válida.')
        sys.exit(1)

    # Obtén el nombre del archivo de salida
    nombre_f, extension = img.rsplit('.', 1)
    nombre_f_trans = f"{nombre_f}_trans.{extension}"

    # Guarda la imagen transformada en un nuevo archivo
    write_img(transform_img, nombre_f_trans)


# Aquí incluyo las transformacions simples que teniamos del anterior programa
def simple(img, transform_f):
    image = read_img(img)
    copy = copy_img(image)

    # Aplicamos la funcion de transformación a la imagen
    if transform_f == 'rotate_right':
        transf = rotate_right(copy)
    elif transform_f == 'mirror':
        transf = mirror(copy)
    elif transform_f == 'blur':
        transf = blur(copy)
    elif transform_f == 'greyscale':
        transf = greyscale(copy)

    # Obtenemos el nombre del archivo de salida
    n_salida, ext = img_name.rsplit('.', 1)
    n_salida_trans = f"{n_salida}_trans.{ext}"

    # Guardamos la imagen transformada en un nuevo archivo
    write_img(transf, n_salida_trans)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Uso: python3 transform_args.py <nombre_archivo> <transformación> <argumentos>')
        sys.exit(1)

    img_name = sys.argv[1]
    transformacion = sys.argv[2]
    # Hacemos una lista para validar las funciones que tenemos y si no, no es válido.
    trans_validas = ['rotate_right', 'mirror', 'blur', 'filter', 'crop', 'greyscale', 'change_colors', 'rotate_colors', 'shift']
    if transformacion not in trans_validas:
        print('Transformación no válida. Intentelo de nuevo.')
        sys.exit(1)
    # Hacemos una condición para que capte si hay argumentos o no y así hacer una u otra función.
    if len(sys.argv) > 3:
        args = list(map(int, sys.argv[3:]))
        main(img_name, transformacion, *args)
    else:
        simple(img_name, transformacion)
